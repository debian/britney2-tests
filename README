The britney test suite
======================

This is the test suite for the Debian Testing Migration Tool, britney.

To run this, you will need:
 * britney (or a clone thereof)
   - including its dependencies :)
 * perl
   - Class::Accessor (libclass-accessor-perl)
   - Dpkg::Control (libdpkg-perl)
   - JSON (libjson-perl)
 * rsync

Running all the tests
---------------------

To run all tests, use:

 $ bin/runtests [options] <britney> <testset> <rundir>

where

 - [options] is a set of optional parameters.
   - Run with "--help" for more information.
 - <britney> is the path to the britney executable.
   - It will be passed some options (see Britney-Style)
 - <testset> is the testset (the t/ dir should do)
 - <rundir> is where the runtime data is dumped
   - this must not exists when running the command above

The return code is 0 on success and 1 on error.

Example

 $ bin/runtests britney2/britney.py t test-out

Running a single test
---------------------

To run a test, use:

 $ bin/runtests [options] <britney> <testset> <rundir> [<testname>|</regex/>]

Where
 - [options], <britney>, <testset> and <rundir> are described in the
   section "Running all the tests" (above).
 - <testname> is the name of the test.
 - </regex/> is a regular expression for matching testnames. Note that the
   regex must start and end with "/".

The return code is 0 on success and 1 on error.

Example:

 $ bin/runtests britney2/britney.py t test-out basic

Understanding a test
--------------------

Some tests will have a description that explains what the test is
about.  If available, it will be in <testset>/<test>/description
which will be a plain text file.


Britney-Style
=============

The test suite natively supports two "britney styles".  The first one
is "britney2" currently used in Debian (on release.debian.org) and the
other is the experimental "SAT-britney".

britney2
--------

The britney2 style is default and is called with:
     -c <conf> --control-files -v

SAT-britney
-----------

The SAT-britney style is enabled by passing '--sat-britney' to the
test runner and it is called with:


     -d <rundir>/<test>/var/data
     --hints-dir <rundir>/<test>/var/data/unstable/Hints
     --heidi <rundir>/<test>/var/data/output/HeidiResult
     -a <archlist>

Note that <archlist> is a comma-separated list of architectures.


Advanced topics
===============

Live-data tests
---------------

We also have tests based on britney's live-data. They are maintained in a
separate Git repository:

	git+ssh://git.debian.org/git/collab-maint/britney-tests-live-data.git

If you would like to use them, you may:

	git submodule update --init

to get the live-data/ directory fetched and updated.

Expected input for britney
--------------------------

 * britney.conf
   - Will be generated (see perl-lib/BritneyTest::_gen_britney_conf)
 * {testing,testing-proposed-updates,unstable}/Sources
 * {testing,testing-proposed-updates,unstable}/BugsV
 * {testing,testing-proposed-updates,unstable}/Hints/*
 * {testing,testing-proposed-updates,unstable}/Packages_$arch
   - one for each architecture
 * FauxPackages
 * testing/Dates
 * testing/Urgency
   

Expected output from britney
----------------------------

 * HEIDI_OUTPUT
   - specified in the britney.conf
   - this is always <rundir>/<test>/var/data/output/HeidiResult

The Heidi output is compared to the "expected" result. The test runner will
generate a diff between the expected and the actual result in

    <rundir>/<test>/diff

if the test fails.

If the test has a file expected-excuses.yaml, the excuses.yaml output is
compared to it, to see if a number of fields produce the same result. If not,
an overview of the difference wil be generated in

    <rundir>/<test>/excuses.diff


Fallback expected output
------------------------

If the Heidi output is different from the "expected" result, it is compared to
the "expected.fallback" result (if that file exists). If will generate a diff
in

    <rundir>/<test>/diff.fallback

If failure of the test is expected, but the result doesn't match the
expected.fallback output, the test will still be marked as failed.


Variants of a test
------------------

Every directory below 'variants' in the test directory is treated as a
separate test. The contents of this subdirectory is copied over the test
directory. This is useful to have multiple versions of the same test, that
only differ in limited ways, like with or without certain options or hints.


Writing a test
--------------

Please refer to README.tests for more information on this topic.
