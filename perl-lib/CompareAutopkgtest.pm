#! /usr/bin/perl

# Copyright 2018-2020 Ivo De Decker <ivodd@debian.org>
# License GPL-2 or (at your option) any later.

package CompareAutopkgtest;

use strict;
use warnings;
use autodie;

use Data::Dumper;
use JSON qw(decode_json encode_json);
use YAML::Syck;

use Exporter qw(import);

our @EXPORT = qw(compare_debci get_debci_issues);

my $issues = ();
my $ok = 1;

sub add_issue {
    my $issue = shift;

    push @$issues, $issue;
    $ok = 0;
}

sub load_debci_info {
    my $file = shift;
    my %lines;
    my @parts;
    my $parsed_json;

    open(my $handle, "<", $file);
    while(<$handle>) {
        @parts = split(/ /, $_, 2);
        $parsed_json = decode_json($parts[1]);
        # debci-testing-amd64:pkg1a {"triggers": ["pkg1a/1.0-2 pkg1b/1.0-2 pkg1c/1.0-2"], "submit-time": "2020-01-01 00:00:00"}
        # strip the submit time from the requests we're testing; it is only for
        # info for people reading the queue
        delete $parsed_json->{'submit-time'} if(exists($parsed_json->{'submit-time'}));
        $lines{"$parts[0] " . encode_json($parsed_json)} = 1;
    }
    close($handle);

    return %lines;
}

sub compare_debci {
    my $debci1 = shift;
    my $debci2 = shift;

    $issues = ();
    $ok = 1;

    my %t1_info = load_debci_info($debci1);
    my %t2_info = load_debci_info($debci2);

	foreach my $k (sort keys %t1_info) {
		unless (defined $t2_info{$k}) {
			$ok = 0;
			add_issue("not in debci2: $k");
		}
	}
	
	foreach my $k (sort keys %t2_info) {
		unless (defined $t1_info{$k}) {
			$ok = 0;
			add_issue("not in debci1: $k");
		}
	}
    return $ok;
}

sub get_debci_issues {
    return $issues;
}


1;
