#! /usr/bin/perl

# Copyright 2018 Ivo De Decker <ivodd@debian.org>
# License GPL-2 or (at your option) any later.

package CompareExcuses;

use strict;
use warnings;
use autodie;

use Data::Dumper;
use YAML::Syck;

use Exporter qw(import);

our @EXPORT = qw(compare_excuses get_excuse_issues);

my $issues = ();
my $ok = 1;

sub add_issue {
    my $issue = shift;

    push @$issues, $issue;
    $ok = 0;
}

sub load_excuses_info {
    my $yamlfile = shift;
    my $ex_info = {};

    my $ex_data = LoadFile($yamlfile);

    foreach my $s (@{$ex_data->{"sources"}}) {
        my $itemn = $s->{"item-name"};
        if (exists($ex_info->{$itemn})) {
            add_issue("duplicate info for $itemn in $yamlfile");
        }
        $ex_info->{$itemn} = $s;
    }

    return $ex_info;
}

sub get_field {
    my $e = shift;
    my $field = shift;

    my @fields = split(m;/;,$field);

    my $r = $e;

    foreach my $f (@fields) {
        if (defined ($r->{$f})) {
            $r = $r->{$f};
        } else {
            return undef;
        }
    }
    return $r;
}

sub compare_field {
    my $field = shift;
    my $expected = shift;
    my $actual = shift;

    my $excuse = $expected->{"item-name"};

    my $f1 = get_field($expected,$field) // "(undef)";
    my $f2 = get_field($actual,$field) // "(undef)";

    if ($f1 ne $f2) {
        add_issue("$excuse $field:  expected: $f1 != actual: $f2");
    }
}

sub compare_policy_info {
    my $expected = shift;
    my $actual = shift;

    my $p1 = $expected->{"policy_info"};
    my $p2 = $actual->{"policy_info"};

    foreach my $k (sort keys %$p1) {
        compare_field("policy_info/$k/verdict",$expected,$actual);
    }

    compare_field("policy_info/age/age-requirement",$expected,$actual);
    compare_field("policy_info/piuparts/test-results",$expected,$actual);
    # TODO bugs (needs array compare)
}

sub compare_excuse {
    my $expected = shift;
    my $actual = shift;

    compare_field("is-candidate",$expected,$actual);
    compare_field("migration-policy-verdict",$expected,$actual);
    compare_field("new-version",$expected,$actual);
    compare_field("old-version",$expected,$actual);
    compare_field("source",$expected,$actual);
    compare_field("component",$expected,$actual); # only for contrib/non-free
    compare_field("manual-approval-status",$expected,$actual);
    compare_field("invalidated-by-other-package",$expected,$actual);
    # some other fields that are currently listed in the excuses.yaml file:
    # TODO reason (needs array compare)
    # TODO forced-reason
    # TODO old-binaries
    # TODO missing-builds
    # TODO hints (currently only removal/block?)
    # TODO dependencies
    #      the data from this is not stable: if there are multiple
    #      dependencies that block an item, one of them might be listed as
    #      'migrate-after' and the other as 'blocked-by', and this might
    #      change on different runs
    #      TODO check if the combination of both is stable

    compare_policy_info($expected,$actual);
}

sub compare_excuses {
    my $excuses1 = shift;
    my $excuses2 = shift;

    $issues = ();
    $ok = 1;

    my $ex1_info = load_excuses_info($excuses1);
    my $ex2_info = load_excuses_info($excuses2);

    foreach my $a (sort grep {!exists $ex1_info->{$_}} keys %$ex2_info) {
        add_issue("not in excuses1: $a");
    }

    foreach my $a (sort keys %$ex1_info) {
        if (!exists $ex2_info->{$a}) {
            add_issue("not in excuses2: $a");
            next;
        }
        compare_excuse($ex1_info->{$a},$ex2_info->{$a});
    }

    return $ok;
}

sub get_excuse_issues {
    return $issues;
}


1;
